-- create Movie(movie_id, movie_title, movie_release_date,movie_time,director_name) 

CREATE TABLE movie (
    movie_id INTEGER,
    movie_title VARCHAR(200),
    movie_release_date VARCHAR(20),
    movie_time VARCHAR(50),
    director_name VARCHAR(200)
)